# language: ru

Функционал: Регистрация пользователя
  Как анонимный пользователь
  Я должен иметь возможность зарегистрироваться в системе
  Когда я введу свои данные

  @register
  Сценарий: Регистрация
    Допустим я захожу на страницу "register"
    Если я ввожу данные:
      | email       | test@test.com |
      | password    | 123qwerty     |
      | displayName | Test Testov   |
    И нажимаю на кнопку "Sign up"
    То я вижу текст "Registered successful!"

  @login
  Сценарий: Логин
    Допустим я захожу на страницу "login"
    Если я ввожу данные:
      | email    | admin@gmail.com |
      | password | 1qaz@WSX29      |
    И нажимаю на кнопку "Sign in"
    То я вижу текст "Login successful"
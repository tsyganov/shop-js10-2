const { I } = inject();
// Add in your custom step files

Given('я захожу на страницу {string}', (page) => {
  I.amOnPage('/' + page);
});

Given('я ввожу данные:', (table) => {
    table.rows.forEach(row => {
        const name = row.cells[0].value;
        const value = row.cells[1].value;
        I.fillField(name, value);
    });
});

When('нажимаю на кнопку {string}', (buttonText) => {
  if (buttonText === 'Sign up' || buttonText === 'Sign in') {
      I.click(`//form//button[.//span[contains(text(), '${buttonText}')]]`);
  } else {
      I.click(buttonText);
  }
});

Then('я вижу текст {string}', (text) => {
    I.see(text);
});

const path = require('path');

const rootPath = __dirname;

const dbHost = process.env.DB_HOST || 'localhost';

let dbUrl = 'mongodb://' + dbHost + '/shop';
let port = 8000;

if (process.env.NODE_ENV === 'test') {
  dbUrl = 'mongodb://' + dbHost + '/shop_test';
  port = 8010;
}

module.exports = {
  rootPath,
  port,
  uploadPath: path.join(rootPath, 'public/uploads'),
  db: {
    url: dbUrl,
  },
  google: {
    clientId: process.env.GOOGLE_CLIENT_ID,
  },
  facebook: {
    appId: process.env.FACEBOOK_APP_ID,
    appSecret: process.env.FACEBOOK_APP_SECRET,
  },
};
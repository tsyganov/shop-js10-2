export let apiURL = 'http://localhost:8000';
export const googleClientId = '810927471031-rp610u0qpftkde3e8u3v9vjeauap6nfc.apps.googleusercontent.com';
export const facebookAppId = process.env.REACT_APP_FACEBOOK_APP_ID;

if (process.env.REACT_APP_ENV === 'test') {
    apiURL = 'http://localhost:8010';
}

if (process.env.NODE_ENV === 'production') {
    apiURL = 'https://shopjs10.ddns.net/api';
}

if (process.env.REACT_APP_API_URL) {
    apiURL = process.env.REACT_APP_API_URL;
}
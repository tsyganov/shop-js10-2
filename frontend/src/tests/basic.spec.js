const add = (a, b) => {
  return a + b;
};

// Описание модуля или класса (того, что мы тестируем)
describe('calculator', () => {
  // Описание функции или метода этого класса
  describe('add', () => {
    // Описание непосредственно проверки
    it('should exist', () => {
      add();
    });

    it('should add 5 + 4 = 9', () => {
      const result = add(5, 4);
      expect(result).toBe(9);
    });

    it('should add 10 + 10 = 20', () => {
      const result = add(10, 10);
      expect(result).toBe(20);
    });
  });
});

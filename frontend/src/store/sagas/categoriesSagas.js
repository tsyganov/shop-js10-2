import {takeEvery} from 'redux-saga/effects';
import {fetchCategoriesRequest, fetchCategoriesFailure, fetchCategoriesSuccess} from "../actions/categoriesActions";
import axiosApi from "../../axiosApi";
import {put} from 'redux-saga/effects';
import {toast} from "react-toastify";

export function* categoriesSagas() {
  try {
    const response = yield axiosApi.get('/categories');
    yield put(fetchCategoriesSuccess(response.data));
  } catch (e) {
    toast.error('Fetch to categories failed');
    yield put(fetchCategoriesFailure());
  }
}

const categoriesSaga = [
  takeEvery(fetchCategoriesRequest, categoriesSagas),
];

export default categoriesSaga;

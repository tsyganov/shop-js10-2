import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link, useLocation} from "react-router-dom";
import {Button, CircularProgress, Grid, makeStyles, Typography} from "@material-ui/core";
import {fetchProducts} from "../../store/actions/productsActions";
import ProductItem from "../../components/ProductItem/ProductItem";
import ProductsLayout from "../../components/UI/Layout/ProductsLayout";

const useStyles = makeStyles(theme => ({
  title: {
    [theme.breakpoints.down('xs')]: {
      marginLeft: "50px",
    },
  }
}));

const Products = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const products = useSelector(state => state.products.products);
  const fetchLoading = useSelector(state => state.products.fetchLoading);
  const user = useSelector(state => state.users.user);
  const search = useLocation().search;

  useEffect(() => {
    dispatch(fetchProducts(search));
  }, [dispatch, search]);

  return (
    <ProductsLayout>
      <Grid container direction="column" spacing={2}>
      <Grid item container justifyContent="space-between" alignItems="center">
        <Grid item className={classes.title}>
          <Typography variant="h4">Products</Typography>
        </Grid>

        {user?.role === 'admin' && (
          <Grid item>
            <Button coloe="primary" component={Link} to="/products/new">Add</Button>
          </Grid>
        )}
      </Grid>
      <Grid item>
        <Grid item container justifyContent="center" direction="row" spacing={1}>
          {fetchLoading ? (
            <Grid container justifyContent="center" alignItems="center">
              <Grid item>
                <CircularProgress/>
              </Grid>
            </Grid>
          ) : products.map(product => (
            <ProductItem
              key={product._id}
              id={product._id}
              title={product.title}
              price={product.price}
              image={product.image}
            />
          ))}
        </Grid>
      </Grid>
    </Grid>
    </ProductsLayout>
  );
};

export default Products;